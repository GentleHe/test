
:stem: latexmath
:icons: font
:source-highlighter: coderay
:sectnums:
:sectlinks:
:sectnumlevels: 4
:toc: left
:toc-title: 目录
:toclevels: 3




//include::MES-Base.adoc[leveloffset=1]


.系统架构总览图
//image::系统架构总览图.svg[role=th]
image::%E7%B3%BB%E7%BB%9F%E6%9E%B6%E6%9E%84%E6%80%BB%E8%A7%88%E5%9B%BE.svg[role=th]
//image::/u7cfb/u7edf/u67b6/u6784/u603b/u89c8/u56fe.svg[role=th]


// 微服务化的内容
//include::MES-Cloud.adoc[leveloffset=-1]




== 参考文章
. https://zhuanlan.zhihu.com/p/357514023[MES系统是什么？云MES是什么？]
. http://www.qykh2009.com/prohelp_1241.html[什么是MES？]
. https://zhuanlan.zhihu.com/p/422702414[MES七大功能模块详解]
. https://zhuanlan.zhihu.com/p/268144625[MES和APS有什么区别？智能工厂布局必须知道]
. https://zhuanlan.zhihu.com/p/68278304[不懂APS系统？十个问答让你对APS瞬间明明白白]
. https://www.qt-asia.com/article/1258.html[什么叫WMS系统？]
. http://t.zoukankan.com/cloudrivers-p-11818994.html[IoT网关协议比较：MQTT,CoAp,XMPP,MBUS,OPC UA]
